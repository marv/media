# Copyright 2008-2018 Wulf C. Krueger <philantrop@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

require python [ blacklist=3 min_versions=2.7.12 multibuild=false ]
require freedesktop-desktop gtk-icon-cache
require github [ user=kovidgoyal tag=v${PV} ]
require systemd-service [ systemd_files=[ calibre-server.service ] ]

export_exlib_phases src_prepare src_compile src_install src_test pkg_postinst pkg_postrm

SUMMARY="Calibre is *the* eBook library management application."
DESCRIPTION="
Formerly known as libprs500, Calibre is an e-book library management application.
It can view, convert and catalog e-books in most of the major e-book formats. It
can also talk to many e-book reader devices. It can go out to the internet and
fetch metadata for your books. It can download newspapers and convert them into
e-books for convenient reading.
Calibre is free, open source and cross-platform in design and works on Linux,
OS X and Windows.
"
HOMEPAGE+=" http://${PN}-ebook.com"
DOWNLOADS="https://download.calibre-ebook.com/${PV}/${PNV}.tar.xz"

BUGS_TO="philantrop@exherbo.org"

UPSTREAM_CHANGELOG="https://github.com/kovidgoyal/${PN}/commits/v${PV}"
UPSTREAM_DOCUMENTATION="${HOMEPAGE}/user_manual/ [[ lang = en ]]"
UPSTREAM_RELEASE_NOTES="${HOMEPAGE}/whats-new"

LICENCES="GPL-3"
SLOT="0"
MYOPTIONS="
    ( providers: libressl openssl ) [[ number-selected = exactly-one ]]
"

# The tests are too much trouble to keep up with
RESTRICT="test"

# Dependencies from: https://github.com/kovidgoyal/build-calibre/blob/master/scripts/sources.json
DEPENDENCIES="
    build:
        dev-lang/nasm[>=2.12.02]
        dev-python/setuptools[>=23.1.0]
        sys-devel/cmake[>=3.6.0]
    build+run:
        app-text/podofo[>=0.9.5]
        app-text/poppler[>=0.52.0]
        dev-db/sqlite:3[>=3.13.0]
        dev-lang/python:2.7[>=2.7.12][sqlite]
        dev-libs/dbus-glib:1[>=0.106]
        dev-libs/expat[>=2.2.4]
        dev-libs/glib:2[>=2.48.1]
        dev-libs/icu:=[>=57.1]
        dev-libs/libffi[>=3.2.1]
        dev-libs/libgcrypt[>=1.7.1]
        dev-libs/libgpg-error[>=1.22]
        dev-libs/libusb:1[>=1.0.20]
        dev-libs/libxml2:2.0[>=2.9.5]
        dev-libs/libxslt[>=1.1.30]
        dev-python/chardet[>=3.0.3]
        dev-python/cssutils[>=1.0.1][python_abis:2.7]
        dev-python/dbus-python[>=1.2.4][python_abis:2.7]
        dev-python/dnspython[>=1.14.0][python_abis:2.7]
        dev-python/dukpy[>=0.3][python_abis:2.7]
        dev-python/html5-parser[>=0.4.3][python_abis:2.7]
        dev-python/html5lib[python_abis:2.7]
        dev-python/lxml[>=3.8.0][python_abis:2.7]
        dev-python/mechanize[>=0.3.5][python_abis:2.7]
        dev-python/msgpack[>=0.4.8][python_abis:2.7]
        dev-python/netifaces[>=0.10.5][python_abis:2.7]
        dev-python/Pillow[>=3.2.0][python_abis:2.7]
        dev-python/psutil[>=4.3.0][python_abis:2.7]
        dev-python/pycrypto[>=2.6.1][python_abis:2.7]
        dev-python/Pygments[>=2.1.3][python_abis:2.7]
        dev-python/PyQt5[>=5.8][webkit][dbus][python_abis:2.7]
        dev-python/python-dateutil[>=2.5.3][python_abis:2.7]
        dev-python/regex[>=2017.07.11][python_abis:2.7]
        dev-python/sip[>=4.19.1][python_abis:2.7]
        dev-python/six[>=1.10.0][python_abis:2.7]
        dev-python/unrardll[>=0.1.3][python_abis:2.7]
        dev-python/webencodings[>=0.5.1][python_abis:2.7]
        media-gfx/optipng[>=0.7.6]
        media-libs/chmlib[>=0.40]
        media-libs/fontconfig[>=2.12.0]
        media-libs/freetype:2[>=2.6.3]
        media-libs/libjpeg-turbo[>=1.5.0]
        media-libs/libmtp[>=1.1.11]
        media-libs/libpng:1.6[>=1.6.23]
        media-libs/libwebp:=[>=0.5.0]
        sys-apps/dbus[>=1.10.8]
        x11-apps/xdg-utils[>=1.0.2]
        x11-libs/libX11
        x11-libs/libXext
        x11-libs/qtbase:5[>=5.6.2]
        x11-libs/qtwebkit:5[>=5.6.2]
        providers:openssl? ( dev-libs/openssl[>=1.0.2j] )
        providers:libressl? ( dev-libs/libressl:= )
    run:
        dev-python/apsw[>=3.13.0][python_abis:2.7]
        fonts/liberation-fonts
    recommendation:
        app-arch/unrar[>=5.5.8] [[ description = [ for unpacking compressed ebooks ] ]]
        app-arch/unzip [[ description = [ for unpacking compressed ebooks ] ]]
        sys-apps/udisks [[ description = [ for using hardware ebook readers ] ]]
"
#        text-libs/libiconv[>=1.14]

# If I ever get my Calibre plugins updated...
#    suggestion:
#        (
#            dev-libs/libplist[>=1.12]
#            app-pda/libusbmuxd[>=1.0.10]
#            app-pda/libimobiledevice[>=1.2.0]
#        ) [[ *description = [ Connect to iOS devices ] *group-name = [ iOS ] ]]

# Calibre bundles a few libs and some of those are rather heavily patched because
# their respective upstreams are unresponsive. The following URL shows ALL
# dependencies not all of which are actually bundled, though:
#
# https://github.com/kovidgoyal/build-calibre/blob/master/scripts/sources.json
#
# Bundled and heavily modified:
#     dev-python/BeautifulSoup[>=3.0.5]
#
# No longer bundled or used (keeping this to check again):
#    dev-python/CherryPy[>=3.1.2-r1][python_abis:2.7]
#    dev-python/cssselect[>=0.7.1]
#    media-libs/libwmf[>=0.2.8.4]

DEFAULT_SRC_PREPARE_PATCHES=(
#    "${FILES}"/0001-test_build.py-Remove-test-failing-in-sandbox.patch
)

prefix="/usr/$(exhost --target)"
bindir="${prefix}/bin"
sharedir="/usr/share"
libdir="${prefix}/lib"

SRC_INSTALL_PARAMS=(
    --prefix="${prefix}"
    --bindir="${bindir}"
    --libdir="${libdir}"
    --sharedir="${sharedir}"
    --root="${IMAGE}"
    --staging-root="${IMAGE}"
    --staging-bindir="${IMAGE}${bindir}"
    --staging-libdir="${IMAGE}${libdir}"
    --staging-sharedir="${IMAGE}${sharedir}"
)

calibre_src_prepare() {
    default

    # get the sysconfig.py and friends for the target
    local target=$(exhost --target)
    export _PYTHON_HOST_PLATFORM=linux-${target%%-*}
    export PYTHONPATH=$(python_get_libdir)
    export SIP_DIR="/usr/share/sip/PyQt5"
}

calibre_src_compile() {
    edo mkdir -p "${WORK}"/resources/localization/locales
    edo mkdir -p "${WORK}"/translations/manual
    edo mkdir -p "${WORK}"/translations/website
    edo mkdir -p "${WORK}"/manual/locale

    edo ${PYTHON} -B setup.py build
    edo ${PYTHON} -B setup.py gui
    edo ${PYTHON} -B setup.py iso3166
    edo ${PYTHON} -B setup.py iso639
    edo ${PYTHON} -B setup.py translations
    edo ${PYTHON} -B setup.py resources
}

calibre_src_test() {
    # Don't run Bonjour tests in a sandbox
    export CI=true

    # network tests would fail anyway so cut them out
    edo rm "${WORK}"/src/calibre/srv/tests/loop.py
    edo rm "${WORK}"/src/calibre/ebooks/oeb/polish/tests/container.py
    edo rm "${WORK}"/src/calibre/srv/tests/http.py
    edo ${PYTHON} -B setup.py test
}

calibre_src_install() {
    local old_XDG="${XDG_DATA_DIRS}"
    local py_site_dir=$(python_get_sitedir)

    install_systemd_files
    insinto /etc/conf.d
    doins "${FILES}"/systemd/calibre-server.conf

    # We have to create these directories before installing because xdg-* needs
    # to see them even if it doesn't use them...
    dodir \
        /etc/xdg/menus \
        /usr/share/applications \
        /usr/share/applnk \
        /usr/share/desktop-directories \
        /usr/share/icons/hicolor \
        /usr/share/mime/packages \
        ${py_site_dir/${prefix}}

    keepdir /var/log/${PN}
    edo chown nobody:nobody "${IMAGE}"/var/log/${PN}

    unset SUDO_UID SUDO_GID SUDO_USER

    export \
        KDEDIRS="${IMAGE}${prefix}" \
        XDG_DATA_DIRS="${IMAGE}${sharedir}" \
        XDG_UTILS_INSTALL_MODE="system" \
        DESTDIR="${IMAGE}"

    edo ${PYTHON} -B setup.py install "${SRC_INSTALL_PARAMS[@]}"

    emagicdocs

    dodir $(python_get_sitedir)

    # Somewhere in the build system this one file gets installed in the wrong place.
    edo mv "${IMAGE}"${py_site_dir/usr\/$(exhost --target)}/init_calibre.py "${IMAGE}"$(python_get_sitedir)/

    python_bytecompile

    export XDG_DATA_DIRS="${old_XDG}"

    # Use system installed liberation-fonts
    edo pushd "${IMAGE}"/${sharedir}/calibre/fonts/liberation
    for fontfile in * ; do
        edo rm ${fontfile}
        dosym /usr/share/fonts/X11/liberation-fonts/${fontfile} ${sharedir}/calibre/fonts/liberation/${fontfile}
    done
    edo popd

    # Removing junk.
    edo rm -r \
        "${IMAGE}"/etc/xdg \
        "${IMAGE}"${sharedir}/{applnk,desktop-directories} \
        "${IMAGE}"${sharedir}/mime/{packages,}

    # Remove bogus stuff.
    edo rm -r \
        "${IMAGE}"/lib

    if [[ -f ${IMAGE}/${sharedir}/applications/defaults.list ]]; then
        edo rm "${IMAGE}"/${sharedir}/applications/defaults.list
    fi

    if [[ -f ${IMAGE}/${sharedir}/applications/mimeinfo.cache ]]; then
        edo rm "${IMAGE}"/${sharedir}/applications/mimeinfo.cache
    fi

    edo find "${IMAGE}"/${sharedir}/applications -type d -empty -delete
}

calibre_pkg_postinst() {
    local cruft=(
        /etc/udev/rules.d/95-calibre.rules
        /etc/bash_completion.d/calibre
        /usr/$(exhost --target)/lib/calibre/calibre/ebooks/chardet
    )

    for file in ${cruft[@]}; do
        if test -f "${file}" ; then
            nonfatal edo rm "${file}" || ewarn "removing ${file} failed"
        elif test -d "${file}" ; then
            nonfatal edo rm -r "${file}" || ewarn "removing directory ${file} failed"
        fi
    done

    freedesktop-desktop_pkg_postinst
    gtk-icon-cache_pkg_postinst
}

calibre_pkg_postrm() {
    freedesktop-desktop_pkg_postrm
    gtk-icon-cache_pkg_postrm
}

