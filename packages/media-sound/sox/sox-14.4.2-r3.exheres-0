# Copyright 2009 Ali Polatel <alip@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

require sourceforge [ suffix=tar.gz ]

SUMMARY="Swiss Army knife of sound processing programs"
DESCRIPTION="
SoX (Sound eXchange) is the Swiss Army knife of sound processing tools: it
can convert sound files between many different file formats & audio devices,
and can apply many sound effects & transformations, as well as doing basic
analysis and providing input to more capable analysis and plotting tools.

SoX is licensed under the GNU GPL and GNU LGPL.  To be precise, the 'sox'
and 'soxi' programs are distributed under the GPL, while the library
'libsox' (in which most of SoX's functionality resides) is dual-licensed.
Note that some optional components of libsox are GPL only: if you use these,
you must use libsox under the GPL.
"

BUGS_TO="alip@exherbo.org"
LICENCES="LGPL-2.1 GPL-2"
SLOT="0"
PLATFORMS="~amd64 ~x86"
MYOPTIONS="alsa ao debug flac id3tag ladspa ogg opus png pulseaudio sndfile wavpack
    mp3 [[ description = [ Support for mp3 encoding with lame, and decoding with mad ] ]]
"

DEPENDENCIES="
    build+run:
        sys-apps/file
        sys-libs/libgomp:= [[ note = [ automagic ] ]]
        alsa? ( sys-sound/alsa-lib )
        ao? ( media-libs/libao )
        flac? ( media-libs/flac )
        id3tag? ( media-libs/libid3tag )
        ladspa? ( media-libs/ladspa-sdk )
        mp3? (
            media-libs/libmad
            media-sound/lame
        )
        ogg? (
            media-libs/libogg
            media-libs/libvorbis
        )
        opus? (
            media-libs/opus
            media-libs/opusfile
        )
        png? ( media-libs/libpng:= )
        pulseaudio? ( media-sound/pulseaudio )
        sndfile? ( media-libs/libsndfile )
        wavpack? ( media-sound/wavpack )
"

DEFAULT_SRC_CONFIGURE_PARAMS=(
    --disable-static
    --with-distro=Exherbo
    --with-ladspa-path=/usr/$(exhost --target)/lib/ladspa
    --with-magic
    --without-amrwb
    --without-amrnb
    --without-coreaudio
    --without-oss
)
DEFAULT_SRC_CONFIGURE_OPTION_ENABLES=( debug )
DEFAULT_SRC_CONFIGURE_OPTION_WITHS=(
    alsa
    ao
    flac
    id3tag
    ladspa
    mp3
    'mp3 lame'
    'mp3 mad'
    'ogg oggvorbis'
    opus
    png
    pulseaudio
    sndfile
    wavpack
)

src_prepare() {
    default

    # NOTE(somasis): fix musl build (disables detecting file-type from pipes on musl only)
    edo sed \
        -e '/#error FIX NEEDED HERE/d'  \
        -i src/formats.c
}

src_install() {
    default
    keepdir /usr/$(exhost --target)/lib/sox
}

